# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Purchase Trainning New',
    'version': '1.0',
    'category': 'Purchases',
    'sequence': 60,
    'summary': 'Purchase orders, tenders and agreements',
    'description': "Trainning odoo",
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['purchase', 'stock', 'product'],
    'data': [
        'views/purchase_views.xml',
        'views/stock_views.xml',
        'views/product_template_views.xml',

        'data/product_data.xml',

        'report/product_report.xml',
        'report/product_report_template.xml',
    ],
    'qweb': [
        'static/src/xml/custom_button.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
