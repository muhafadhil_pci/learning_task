from odoo import models,fields,api,_
from datetime import datetime

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    # TASK 1
    supplier_phone = fields.Char(related='partner_id.phone', string='Phone')
    supplier_email = fields.Char(related='partner_id.email', String='Email', widget='email')

    # TASK 2
    contact_person = fields.Many2one('res.users', String='Contact Person', default=lambda self: self.env.uid, readonly=True)

    # TASK 3
    duration_days = fields.Float('Duration', compute='compute_duration')

    @api.depends('date_planned', 'date_order')
    def compute_duration(self):
        #this 
        self.duration_days = (self.date_planned - self.date_order).days

    # TASK 4      
    @api.multi
    def action_view_related_products(self):
        product = []
        for i in self.order_line:
            product.append(i.product_id.id)
        return {
            'type': 'ir.actions.act_window',
            'name': _('Chart of Products'),
            'res_model': 'product.product',
            'view_mode': 'tree',
            'target': 'new',
            'views_id': False,
            'domain': [('id', 'in', product)],
        }
    
    # TASK 6
    partner_ref = fields.Char('Vendor Reference', copy=False,
        help="Reference of the sales order or bid sent by the vendor. "
             "It's used to do the matching when you receive the "
             "products as this reference is usually written on the "
             "delivery order sent by your vendor.")
    
    @api.model
    def create(self, vals):
        if not self.partner_ref:
            vals['partner_ref'] = "No Supplier Reference"
        return super(PurchaseOrder, self).create(vals)
    
    @api.multi
    def write(self, values):
        if not self.partner_ref:
            values['partner_ref'] = "No Supplier Reference"
        return super(PurchaseOrder, self).write(values)

    
    # TASK 9
    down_payment = fields.Float('Down Payment (DP)')
    amount_total_dp = fields.Float('Total', readonly=True, compute='amount_dp')

    @api.depends('amount_untaxed')
    def amount_dp(self):
        au = self.amount_untaxed
        dp = self.down_payment
        total = au - dp
        print(total)
        self.amount_total_dp = total






    