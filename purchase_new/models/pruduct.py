from odoo import models,fields,api,_
from datetime import datetime
from odoo.exceptions import UserError

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    # TASK 7 & 10
    
    @api.model
    def create(self, vals):
        record = self.env.ref('purchase_new.no_supplier')
        if not vals.get('seller_ids', False):
            vals['seller_ids'] = [(0, 0, {'name': record.id})]
            raise UserError(_("You should fill in the supplier details, at least one."))
        return super(ProductTemplate, self).create(vals)

    
    @api.multi
    def write(self, values):
        record = self.env.ref('purchase_new.no_supplier')
        if not values.get('seller_ids', False):
            values['seller_ids'] = [(0, 0, {'name': record.id})]
            raise UserError(_("You should fill in the supplier details, at least one."))
        else:
            continue
        return super(ProductTemplate, self).write(values)