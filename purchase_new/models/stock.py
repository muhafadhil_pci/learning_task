from odoo import models,fields,api,_
from datetime import datetime

class StockPicking (models.Model):
    _inherit = "stock.picking"

    # TASK 5
    confirm_date = fields.Datetime('Confirmed Date')
    confirm_user = fields.Many2one('res.users', String='Confirmed by', readonly=True)

    @api.multi
    def action_confirm(self):
        record = super(StockPicking, self).action_confirm()
        self.confirm_date = datetime.now()
        self.confirm_user = self.env.uid
        return record