# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Purchase Trainning',
    'version': '1.0',
    'category': 'Purchases',
    'sequence': 60,
    'summary': 'Purchase orders, tenders and agreements',
    'description': "Trainning odoo",
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['purchase'],
    'data': [
        'views/purchase_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
