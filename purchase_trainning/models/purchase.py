from odoo import models,fields,api,_

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    supplier_phone = fields.Char(related='partner_id.phone', string='Phone')
    supplier_email = fields.Char(related='partner_id.email', String='Email', widget='email')
