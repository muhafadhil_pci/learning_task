from odoo import models,fields,api,_
from datetime import datetime
from odoo.exceptions import UserError


class LoanOrder (models.Model):
    _name = "loan.order"
    

    name = fields.Char(string='LO Number', required=True, copy=False, readonly=True, index=True,  default=lambda self: _('New'))
    partner_id = fields.Many2one('res.partner', string="Customer Name")
    order_date = fields.Datetime('Loan Date', required=True, index=True, copy=False, default=fields.Datetime.now)
    starting_date = fields.Datetime('Starting Date', index=True, copy=False)
    end_date = fields.Datetime('Ending Date', index=True, copy=False)
    status = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('partially delivered', 'Partially Delivered'),
        ('delivered', 'Delivered'),
        ('partially returned', 'Partially Returned'),
        ('returned', 'Returned')
        ], string='Loan Status', store=True)
    loan_order_ids = fields.One2many('loan.order.line', 'loan_order_id', string='Order Lines')

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            if 'company_id' in vals:
                vals['name'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code('loan.order') or _('New')
            else:
                vals['name'] = self.env['ir.sequence'].next_by_code('loan.order') or _('New')
        result = super(LoanOrder, self).create(vals)
        return result
    

    class LoanOrderLine(models.Model):
        _name = "loan.order.line"

        product_id = fields.Many2one('product.product', string="Product")
        name = fields.Char(string='Description', required=True)
        qty = fields.Float('Quantity', required=True, default=1.0)
        loan_order_id = fields.Many2one('loan.order')

        @api.onchange('product_id')
        def product_id_change(self):
            for i in self:
                if i.product_id:
                    i.name = " [" + i.product_id.default_code + "] "+ i.product_id.name

