# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Loan Order',
    'version': '1.0',
    'category': 'Sale',
    'sequence': 60,
    'summary': '',
    'description': "Trainning odoo",
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['sale', 'stock', 'product'],
    'data': [
        'security/loan_security.xml',
        'security/ir.model.access.csv',
        'views/loan_order_view.xml',
        'data/ir_sequence_data.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}