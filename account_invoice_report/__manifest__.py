# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Account Trainning',
    'version': '1.0',
    'category': 'Account',
    'sequence': 60,
    'summary': 'Account',
    'description': "Trainning odoo",
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['account', 'sale_management', 'stock'],
    'data': [
        'report/report_invoice.xml',
        'report/report_invoice_template.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}