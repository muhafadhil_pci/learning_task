from odoo import models,fields,api,_
from datetime import datetime
from odoo.exceptions import UserError


class ProductEcomerce(models.Model):
    _inherit = "product.template"

    stock_minimum = fields.Integer(String="Stock Minimum", default=1)

    # def _stock(self):
    #     if self.stock_minimun < 1:
    #         raise UserError(_("Stock minimum must be fill more than 0"))
    @api.model
    def create(self, vals):
        if self.stock_minimum:
            if vals['stock_minimum'] < 1:
                raise UserError(_("Stock minimum must be fill more than 0"))
        return super(ProductEcomerce, self).create(vals)
    
    @api.multi
    def write(self, values):
        if self.stock_minimum:
            if values['stock_minimum'] < 1:
                raise UserError(_("Stock minimum must be fill more than 0"))
        return super(ProductEcomerce, self).write(values)