# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Ecommerce Web Modify',
    'version': '1.0',
    'category': '',
    'sequence': 60,
    'summary': '',
    'description': "Trainning odoo",
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['product', 'website_sale', 'website'],
    'data': [
        'views/product_view.xml',
        'views/template.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
