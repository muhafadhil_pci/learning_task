# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Invoice Appraisal',
    'version': '1.0',
    'category': 'Invoicing Management',
    'sequence': 60,
    'summary': '',
    'description': "Trainning odoo",
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['account'],
    'data': [
        'wizard/invoice_state_view.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
