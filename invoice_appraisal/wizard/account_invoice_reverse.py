# -*- coding: utf-8 -*-
from odoo import models, api, _
from odoo.exceptions import UserError


class AccountInvoiceReverse(models.TransientModel):
    """
    This wizard will confirm the all the selected draft invoices
    """

    _name = "account.invoice.reverse"
    _inherit = ["account.invoice.refund", "account.unreconcile"]

    
    @api.multi
    def invoice_refund(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for record in self.env['account.invoice'].browse(active_ids):
            if record.state == 'paid':
                line_ids = record.mapped('move_id').mapped('line_ids')
                line_ids.remove_move_reconcile()
                record.action_invoice_cancel()
        return res